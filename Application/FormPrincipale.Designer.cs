﻿namespace SynapseApplication
{
    partial class FormPrincipale
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStripFormPrincipale = new System.Windows.Forms.MenuStrip();
            this.pROJETToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nouveauToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.consulterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rechercherToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mISSIONToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nouvelleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rechercherToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.iNTERVENANTToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nouveauToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.rechercherToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStripFormPrincipale.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStripFormPrincipale
            // 
            this.menuStripFormPrincipale.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pROJETToolStripMenuItem,
            this.mISSIONToolStripMenuItem,
            this.iNTERVENANTToolStripMenuItem});
            this.menuStripFormPrincipale.Location = new System.Drawing.Point(0, 0);
            this.menuStripFormPrincipale.Name = "menuStripFormPrincipale";
            this.menuStripFormPrincipale.Size = new System.Drawing.Size(800, 24);
            this.menuStripFormPrincipale.TabIndex = 1;
            this.menuStripFormPrincipale.Text = "menuStripFormPrincipale";
            // 
            // pROJETToolStripMenuItem
            // 
            this.pROJETToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.nouveauToolStripMenuItem,
            this.consulterToolStripMenuItem,
            this.rechercherToolStripMenuItem});
            this.pROJETToolStripMenuItem.Name = "pROJETToolStripMenuItem";
            this.pROJETToolStripMenuItem.Size = new System.Drawing.Size(59, 20);
            this.pROJETToolStripMenuItem.Text = "PROJET";
            // 
            // nouveauToolStripMenuItem
            // 
            this.nouveauToolStripMenuItem.Name = "nouveauToolStripMenuItem";
            this.nouveauToolStripMenuItem.Size = new System.Drawing.Size(133, 22);
            this.nouveauToolStripMenuItem.Text = "Nouveau";
            // 
            // consulterToolStripMenuItem
            // 
            this.consulterToolStripMenuItem.Name = "consulterToolStripMenuItem";
            this.consulterToolStripMenuItem.Size = new System.Drawing.Size(133, 22);
            this.consulterToolStripMenuItem.Text = "Consulter";
            // 
            // rechercherToolStripMenuItem
            // 
            this.rechercherToolStripMenuItem.Name = "rechercherToolStripMenuItem";
            this.rechercherToolStripMenuItem.Size = new System.Drawing.Size(133, 22);
            this.rechercherToolStripMenuItem.Text = "Rechercher";
            // 
            // mISSIONToolStripMenuItem
            // 
            this.mISSIONToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.nouvelleToolStripMenuItem,
            this.rechercherToolStripMenuItem1});
            this.mISSIONToolStripMenuItem.Name = "mISSIONToolStripMenuItem";
            this.mISSIONToolStripMenuItem.Size = new System.Drawing.Size(66, 20);
            this.mISSIONToolStripMenuItem.Text = "MISSION";
            // 
            // nouvelleToolStripMenuItem
            // 
            this.nouvelleToolStripMenuItem.Name = "nouvelleToolStripMenuItem";
            this.nouvelleToolStripMenuItem.Size = new System.Drawing.Size(133, 22);
            this.nouvelleToolStripMenuItem.Text = "Nouvelle";
            // 
            // rechercherToolStripMenuItem1
            // 
            this.rechercherToolStripMenuItem1.Name = "rechercherToolStripMenuItem1";
            this.rechercherToolStripMenuItem1.Size = new System.Drawing.Size(133, 22);
            this.rechercherToolStripMenuItem1.Text = "Rechercher";
            // 
            // iNTERVENANTToolStripMenuItem
            // 
            this.iNTERVENANTToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.nouveauToolStripMenuItem1,
            this.rechercherToolStripMenuItem2});
            this.iNTERVENANTToolStripMenuItem.Name = "iNTERVENANTToolStripMenuItem";
            this.iNTERVENANTToolStripMenuItem.Size = new System.Drawing.Size(97, 20);
            this.iNTERVENANTToolStripMenuItem.Text = "INTERVENANT";
            // 
            // nouveauToolStripMenuItem1
            // 
            this.nouveauToolStripMenuItem1.Name = "nouveauToolStripMenuItem1";
            this.nouveauToolStripMenuItem1.Size = new System.Drawing.Size(133, 22);
            this.nouveauToolStripMenuItem1.Text = "Nouveau";
            // 
            // rechercherToolStripMenuItem2
            // 
            this.rechercherToolStripMenuItem2.Name = "rechercherToolStripMenuItem2";
            this.rechercherToolStripMenuItem2.Size = new System.Drawing.Size(133, 22);
            this.rechercherToolStripMenuItem2.Text = "Rechercher";
            // 
            // FormPrincipale
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.menuStripFormPrincipale);
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStripFormPrincipale;
            this.Name = "FormPrincipale";
            this.Text = "SYNAPSE APPLICATION";
            this.menuStripFormPrincipale.ResumeLayout(false);
            this.menuStripFormPrincipale.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStripFormPrincipale;
        private System.Windows.Forms.ToolStripMenuItem pROJETToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nouveauToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem consulterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rechercherToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mISSIONToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nouvelleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rechercherToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem iNTERVENANTToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nouveauToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem rechercherToolStripMenuItem2;
    }
}

